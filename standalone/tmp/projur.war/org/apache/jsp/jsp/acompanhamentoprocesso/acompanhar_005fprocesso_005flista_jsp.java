/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: WildFly 2.2.0.Final - 1.4.0.Final
 * Generated at: 2020-07-09 18:19:35 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.jsp.acompanhamentoprocesso;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import projur.moduloAcompanhamentoProcesso.processo.Processo;
import projur.moduloAcompanhamentoProcesso.tabelasBasicas.fase.Fase;
import java.util.*;
import java.text.SimpleDateFormat;
import projur.comum.util.StringTools;

public final class acompanhar_005fprocesso_005flista_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("java.util");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = new java.util.HashSet<>();
    _jspx_imports_classes.add("projur.comum.util.StringTools");
    _jspx_imports_classes.add("projur.moduloAcompanhamentoProcesso.tabelasBasicas.fase.Fase");
    _jspx_imports_classes.add("java.text.SimpleDateFormat");
    _jspx_imports_classes.add("projur.moduloAcompanhamentoProcesso.processo.Processo");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

final java.lang.String _jspx_method = request.getMethod();
if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JBWEB004248: JSPs only permit GET POST or HEAD");
return;
}

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      response.addHeader("X-Powered-By", "JSP/2.3");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<script language=\"JavaScript\" src=\"scripts/grid.js\"></script>\r\n");
      out.write("<script language=\"JavaScript\" src=\"scripts/funcoes.js\"></script>\r\n");
      out.write("<link href=\"css/formularios.css\" rel=\"stylesheet\" type=\"text/css\">\r\n");
      out.write("<link href=\"css/grid.css\" rel=\"stylesheet\" type=\"text/css\"></link>\r\n");
      out.write("<style type=\"text/css\">\r\n");
      out.write("<!--\r\n");
      out.write("body {\r\n");
      out.write("\tmargin-left: 0px;\r\n");
      out.write("\tmargin-top: 0px;\r\n");
      out.write("\tmargin-right: 0px;\r\n");
      out.write("\tmargin-bottom: 0px;\r\n");
      out.write("}\r\n");
      out.write("-->\r\n");
      out.write("</style>\r\n");
      out.write("<body topmargin=\"0\" leftmargin=\"0\">\r\n");
      out.write("\r\n");
 Collection colecaoProcessos = null;
   // Verificamos se os processos foram incluidos na sessão
   if ( request.getAttribute( "colecaoProcessos" ) != null ){
     colecaoProcessos = (Collection) request.getAttribute( "colecaoProcessos" );
   }
  String mensagem = (String) request.getAttribute( "mensagem" );
  
  String dtIni = (String) request.getAttribute( "txtDataIni" );
  String dtFim = (String) request.getAttribute( "txtDataFim" );
  

      out.write("\r\n");
      out.write("\r\n");

  String[] colunas = {"Nr. Processo","Descrição da ação","Advogado Responsável", "Escritorio Terceiro", "Número do SIJUS"};
  String[] larguras = {"70","140","140","140","100"};
  String[] alinhamento = {"left","left","left","left","left"};
  String[] tipos = {"string","string","string","string", "string", "string"};
  String[][] dados = new String[0][0];
  String[] arrLinks = new String[0];
  int altura = 95;
  //Atributo usado na paginação
  StringBuffer paginacao = new StringBuffer("");

  if ( colecaoProcessos != null ){
    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");

    Processo[] arrProcessos = new Processo[colecaoProcessos.size()];
    colecaoProcessos.toArray( arrProcessos );

    String formato = "dd/MM/yyyy";
    SimpleDateFormat formatter = new SimpleDateFormat(formato);

    dados = new String[arrProcessos.length][colunas.length];
    arrLinks = new String[arrProcessos.length];
    
    String descAux = null;

    for(int i=0; i < arrProcessos.length; i++){
       dados[i][0] = arrProcessos[i].getNumeroProcesso()+"";
       dados[i][1] = (arrProcessos[i].getDescricaoAcao() != null && !"".equalsIgnoreCase( arrProcessos[i].getDescricaoAcao() ) )?arrProcessos[i].getDescricaoAcao():"-";
       dados[i][2] = arrProcessos[i].getAdvogadoChesf().getNome();
       dados[i][3] = arrProcessos[i].getEscritorioTerceiro().getNomeEscritorio();
	   dados[i][4] = StringTools.formataNumeroSisJus( arrProcessos[i] );

	   descAux = ((arrProcessos[i].getDescricaoAcao() != null)?arrProcessos[i].getDescricaoAcao().replace("'","´"):"");

       arrLinks[i] = "parent.mudaEstadoSegundoPasso( true, '" + arrProcessos[i].getCodigoProcesso() + "', '" + descAux + "','"+StringTools.formataNumeroSisJus( arrProcessos[i] )+"', '" + arrProcessos[i].getNumeroProcesso() + "', '" + arrProcessos[i].getRisco() +"' );";
    }
  }

  // Colecao com todas as fase cadastradas
  Collection colecaoFases = (Collection) request.getAttribute( "fases" );

  if ( colecaoFases != null && colecaoFases.size() > 0 ){
    Fase[] arrFase = new Fase[colecaoFases.size()];
    colecaoFases.toArray( arrFase );


      out.write("  <script language=\"JavaScript\">");


    for ( int i=0; i < arrFase.length; i++ ){
      String nome = arrFase[i].getNomeFase();
      String id = String.valueOf( arrFase[i].getCodigoFase() );
      out.write("\r\n");
      out.write("\r\n");
      out.write("      newoption = new Option('");
      out.print(nome);
      out.write("', '");
      out.print(id);
      out.write("', 'false', 'false');\r\n");
      out.write("      parent.document.form1.slFases.options[");
      out.print(i);
      out.write("] = newoption;\r\n");
  } 
      out.write("\r\n");
      out.write("\r\n");
      out.write("    </script>\r\n");
      out.write("\r\n");
  }

  out.print(StringTools.getGrid(colunas,larguras,alinhamento,tipos,dados,arrLinks,altura, 20));
  
  
  //A partir daqui eh implementação da paginação de registros.
  //Atributos para auxilio a paginação..
  String numProcessos = (String)request.getAttribute( "numProcessos" );
  final int numRegPorPag = 10; //Determina a quantidade de registros que será mostrado por página
  String numPaginaAtual = request.getParameter("numPagina");
	//Se o valor da página atual diferente de nulo e vazio significa que o usuário clicou no botão pesquisar
	//e a página atual será 1
  if(numPaginaAtual != null){
	  if(numPaginaAtual.equals("")){
		  numPaginaAtual = "1";
	  }
  }

	int numPaginas = 0;

  if(numProcessos != null && !numProcessos.trim().equals("")){
		numPaginas = Integer.parseInt(numProcessos)/numRegPorPag;
		//Seta a quantidade de páginas de acordo com  o número total de registros.
		if(numPaginas%numRegPorPag == 0){
			numPaginas = numPaginas;
		}else{
			numPaginas = numPaginas + 1;
		}

//Escrevendo no HTML.. os números da paginaçao
	paginacao.append("<table><tr><td>Número Total de Registros: " + numProcessos + "</td></tr>");
    paginacao.append("<tr><td>Página Atual: " + numPaginaAtual + "</td></tr>");
    paginacao.append("<tr><td></td><td>");
	//Link para menos dez páginas.
	if( Integer.parseInt(numPaginaAtual) > 10 ){
      if(numPaginas != 0){
        paginacao.append("<a href=\"javascript:pesquisaProcesso(" + (Integer.parseInt(numPaginaAtual) - 10) + ");\">-10&nbsp;&nbsp;</a>");
      }
    }
    //Link para pagina anterior a atual..case esteja na primeira nem escreve
    if(Integer.parseInt(numPaginaAtual) != 1){
      paginacao.append("<a href=\"javascript:pesquisaProcesso(" + (Integer.parseInt(numPaginaAtual) - 1) + ");\">Anterior</a>");
    }
    //Links com as páginas...
    for(int i = Integer.parseInt(numPaginaAtual); i<=numPaginas && i <= (Integer.parseInt(numPaginaAtual)+10); i++){
      if(Integer.parseInt(numPaginaAtual) != i){
        paginacao.append("<a href=\"javascript:pesquisaProcesso(" + i + ");\">  " + i + "  </a>");
      }else{
        paginacao.append("  "+i+"  ");
      }
    }//end-for");
    //Link para a próxima página...
    if(Integer.parseInt(numPaginaAtual) != numPaginas){
      if(numPaginas != 0){
        paginacao.append("<a href=\"javascript:pesquisaProcesso(" + (Integer.parseInt(numPaginaAtual) + 1) + ");\"> Próximo</a>");
      }
    }
	//Link para mais dez páginas.
	if(Integer.parseInt(numPaginaAtual)  < (numPaginas - 10)){
      if(numPaginas != 0){
        paginacao.append("<a href=\"javascript:pesquisaProcesso(" + (Integer.parseInt(numPaginaAtual) + 10) + ");\">&nbsp;&nbsp;+10 </a>");
      }
    }
    paginacao.append("</td><td></td></tr>");
    paginacao.append("</table>");
  }//end-if-maior;
  
  

      out.write("\r\n");
      out.write("<script>\r\n");
      out.write("  ");

  //Se o sistema retornou alguma mensagem, exiba na janela pai
  if(mensagem != null){
  	
      out.write("parent.mensagemSistema.innerText = '");
      out.print(mensagem);
      out.write('\'');
      out.write(';');

  }
  
      out.write("\r\n");
      out.write("  habilitaForm(parent.document.form1);\r\n");
      out.write("  parent.iframe_acompanhar_processo_lista.height = \"100px\";\r\n");
      out.write("  parent.habilitaControles();\r\n");
      out.write("  parent.div_pg.innerHTML = '");
      out.print(paginacao.toString());
      out.write("';\r\n");
      out.write("  parent.form1.numProcessos.value = '");
      out.print(numProcessos);
      out.write("';\r\n");
      out.write("  parent.form1.numRegPorPag.value = '");
      out.print(numRegPorPag);
      out.write("';\r\n");
      out.write("  ");
 
  if( dtIni != null && dtFim != null ){
  
      out.write("\r\n");
      out.write("  \r\n");
      out.write("  parent.form1.txtDataIni.value = '");
      out.print(dtIni);
      out.write("';\r\n");
      out.write("  parent.form1.txtDataFim.value = '");
      out.print(dtFim);
      out.write("';\r\n");
      out.write("  \r\n");
      out.write("  ");

  }
  
      out.write("\r\n");
      out.write("  parent.jaClicou = false;\r\n");
      out.write("</script>\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
