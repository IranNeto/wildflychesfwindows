/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: WildFly 2.2.0.Final - 1.4.0.Final
 * Generated at: 2020-07-09 19:37:01 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.jsp.acompanhamentoprocesso;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import java.text.*;
import projur.comum.util.*;
import java.text.SimpleDateFormat;
import projur.comum.util.StringTools;
import projur.moduloAcompanhamentoProcesso.processo.*;

public final class autorizar_005fandamento_005fprocesso_005flista_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("java.util");
    _jspx_imports_packages.add("java.text");
    _jspx_imports_packages.add("projur.moduloAcompanhamentoProcesso.processo");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("projur.comum.util");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = new java.util.HashSet<>();
    _jspx_imports_classes.add("projur.comum.util.StringTools");
    _jspx_imports_classes.add("java.text.SimpleDateFormat");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

final java.lang.String _jspx_method = request.getMethod();
if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JBWEB004248: JSPs only permit GET POST or HEAD");
return;
}

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      response.addHeader("X-Powered-By", "JSP/2.3");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n");
      out.write("<script language=\"JavaScript\" src=\"scripts/grid.js\"></script>\n");
      out.write("<script language=\"JavaScript\" src=\"scripts/funcoes.js\"></script>\n");
      out.write("<link href=\"css/formularios.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("<link href=\"css/grid.css\" rel=\"stylesheet\" type=\"text/css\"></link>\n");
      out.write("<style type=\"text/css\">\n");
      out.write("    <!--\n");
      out.write("    body {\n");
      out.write("        margin-left: 0px;\n");
      out.write("        margin-top: 0px;\n");
      out.write("        margin-right: 0px;\n");
      out.write("        margin-bottom: 0px;\n");
      out.write("    }\n");
      out.write("    -->\n");
      out.write("</style>\n");
      out.write("<body topmargin=\"0\" leftmargin=\"0\">\n");
      out.write("\n");
 Collection colecaoProcessos = null;
    // Verificamos se os processos foram incluidos na sessão
    if ( request.getAttribute( "colecaoProcessos" ) != null ){
        colecaoProcessos = (Collection) request.getAttribute( "colecaoProcessos" );
    }
    String mensagem = (String) request.getAttribute( "mensagem" );

    String dtIni = (String) request.getAttribute( "txtDataIni" );
    String dtFim = (String) request.getAttribute( "txtDataFim" );

    String[] colunas = {"Data Solicitação", "Nr. Sijus", "Nr. Processo", "Gerente Responsável", "Advogado Responsável",
            "Solicitante", "Situação", "Êxito anterior", "Êxito", "Fase", "Data Andamento", "Descrição", "Data Resposta"};
    String[] larguras = {"80","80","140","150","150","150", "80", "80", "80", "80", "80", "150", "80"};
    String[] alinhamento = {"left","left","left","right","left","left","left","left", "left","left","left", "left", "left q"};
    String[] tipos = {"string","string","string","string", "string", "string", "string", "string", "string", "string", "string", "string", "string"};
    String[][] dados = new String[0][0];
    String[] arrLinks = new String[0];
    int altura = 95;
    //Atributo usado na paginação
    StringBuffer paginacao = new StringBuffer("");

    if ( colecaoProcessos != null ){

        String formato = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(formato);

        dados = new String[colecaoProcessos.size()][colunas.length];
        arrLinks = new String[colecaoProcessos.size()];

        Iterator itProc = colecaoProcessos.iterator();

        int i = 0;
        while(itProc.hasNext()) {
            RevisaoAndamento revisaoAndamento = (RevisaoAndamento) itProc.next();
            dados[i][0] = ( revisaoAndamento.getDataSolicitacao() != null ) ? formatter.format( revisaoAndamento.getDataSolicitacao() ) : " ";
            dados[i][1] = revisaoAndamento.getNumeroSijusProcesso();
            dados[i][2] = revisaoAndamento.getNumeroProcesso();
            dados[i][3] = revisaoAndamento.getNomeGerente();
            dados[i][4] = revisaoAndamento.getNomeAdvogado();
            dados[i][5] = revisaoAndamento.getNomeSolicitante();
            dados[i][6] = revisaoAndamento.getDescricaoStatus();
            dados[i][7] = revisaoAndamento.getRiscoAnterior();
            dados[i][8] = revisaoAndamento.getRisco();
            dados[i][9] = revisaoAndamento.getNomeFase();
            dados[i][10] = revisaoAndamento.getDataAndamento() != null ? formatter.format(revisaoAndamento.getDataAndamento()) : " ";
            dados[i][11] = revisaoAndamento.getDescricaoAndamento();
            dados[i][12] = ( revisaoAndamento.getDataResposta() != null ) ? formatter.format( revisaoAndamento.getDataResposta() ) : " ";

            arrLinks[i] = "parent.mudaEstadoSegundoPasso( true, '" + revisaoAndamento.getCodigoRevisao() + "','" + revisaoAndamento.getCodigoProcesso() + "','" + revisaoAndamento.getIdStatus() + "');";

            i++;
        }
    }


    out.print(StringTools.getGrid(colunas,larguras,alinhamento,tipos,dados,arrLinks,altura, 20));

    //A partir daqui eh implementação da paginação de registros.
    //Atributos para auxilio a paginação..
    String numProcessos = (String)request.getAttribute( "numProcessos" );
    final int numRegPorPag = 10; //Determina a quantidade de registros que será mostrado por página
    String numPaginaAtual = request.getParameter("numPagina");
    //Se o valor da página atual diferente de nulo e vazio significa que o usuário clicou no botão pesquisar
    //e a página atual será 1
    if(numPaginaAtual != null){
        if(numPaginaAtual.equals("")){
            numPaginaAtual = "1";
        }
    }

    int numPaginas = 0;

    if(numProcessos != null && !numProcessos.trim().equals("")){
        numPaginas = Integer.parseInt(numProcessos)/numRegPorPag;
        //Seta a quantidade de páginas de acordo com  o número total de registros.
        if(numPaginas%numRegPorPag == 0){
            numPaginas = numPaginas;
        }else{
            numPaginas = numPaginas + 1;
        }

//Escrevendo no HTML.. os números da paginaçao
        paginacao.append("<table><tr><td>Número Total de Registros: " + numProcessos + "</td></tr>");
        paginacao.append("<tr><td>Página Atual: " + numPaginaAtual + "</td></tr>");
        paginacao.append("<tr><td></td><td>");
        //Link para menos dez páginas.
        if( Integer.parseInt(numPaginaAtual) > 10 ){
            if(numPaginas != 0){
                paginacao.append("<a href=\"javascript:pesquisaProcesso(" + (Integer.parseInt(numPaginaAtual) - 10) + ");\">-10&nbsp;&nbsp;</a>");
            }
        }
        //Link para pagina anterior a atual..case esteja na primeira nem escreve
        if(Integer.parseInt(numPaginaAtual) != 1){
            paginacao.append("<a href=\"javascript:pesquisaProcesso(" + (Integer.parseInt(numPaginaAtual) - 1) + ");\">Anterior</a>");
        }
        //Links com as páginas...
        for(int i = Integer.parseInt(numPaginaAtual); i<=numPaginas && i <= (Integer.parseInt(numPaginaAtual)+10); i++){
            if(Integer.parseInt(numPaginaAtual) != i){
                paginacao.append("<a href=\"javascript:pesquisaProcesso(" + i + ");\">  " + i + "  </a>");
            }else{
                paginacao.append("  "+i+"  ");
            }
        }//end-for");
        //Link para a próxima página...
        if(Integer.parseInt(numPaginaAtual) != numPaginas){
            if(numPaginas != 0){
                paginacao.append("<a href=\"javascript:pesquisaProcesso(" + (Integer.parseInt(numPaginaAtual) + 1) + ");\"> Próximo</a>");
            }
        }
        //Link para mais dez páginas.
        if(Integer.parseInt(numPaginaAtual)  < (numPaginas - 10)){
            if(numPaginas != 0){
                paginacao.append("<a href=\"javascript:pesquisaProcesso(" + (Integer.parseInt(numPaginaAtual) + 10) + ");\">&nbsp;&nbsp;+10 </a>");
            }
        }
        paginacao.append("</td><td></td></tr>");
        paginacao.append("</table>");
    }//end-if-maior;

      out.write("\n");
      out.write("<script>\n");
      out.write("    ");

    //Se o sistema retornou alguma mensagem, exiba na janela pai
    if(mensagem != null){
        
      out.write("parent.mensagemSistema.innerText = '");
      out.print(mensagem);
      out.write('\'');
      out.write(';');

    }
  
      out.write("\n");
      out.write("    habilitaForm(parent.document.form1);\n");
      out.write("    parent.iframe_autorizar_andamento_processo_lista.height = \"300px\";\n");
      out.write("    ");

    if(colecaoProcessos.size() > numRegPorPag) {
    
      out.write("\n");
      out.write("       //parent.div_pg.innerHTML = '");
      out.print(paginacao.toString());
      out.write("';\n");
      out.write("    ");

    }
    
      out.write("\n");
      out.write("    parent.habilitaControles();\n");
      out.write("    parent.form1.numProcessos.value = '");
      out.print(numProcessos);
      out.write("';\n");
      out.write("    parent.form1.numRegPorPag.value = '");
      out.print(numRegPorPag);
      out.write("';\n");
      out.write("    ");

    if( dtIni != null && dtFim != null ){
    
      out.write("\n");
      out.write("\n");
      out.write("    parent.form1.txtDataIni.value = '");
      out.print(dtIni);
      out.write("';\n");
      out.write("    parent.form1.txtDataFim.value = '");
      out.print(dtFim);
      out.write("';\n");
      out.write("\n");
      out.write("    ");

    }
    
      out.write("\n");
      out.write("    parent.jaClicou = false;\n");
      out.write("</script>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
